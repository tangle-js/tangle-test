const json5 = require('json5')
const isEqual = require('lodash.isequal')
const Graph = require('@tangle/graph')

const ARROW_PATTERN = /\s*-*>\s*/

function buildNodes (str, opts = {}) {
  const links = str.trim().split('\n').reduce(
    (acc, line) => {
      line.trim().split(ARROW_PATTERN)
        .forEach((from, i, nodes) => {
          if (i + 1 === nodes.length) return

          acc.push({
            from: parseKeyValue(from),
            to: parseKeyValue(nodes[i + 1])
          })
        })
      return acc
    },
    []
  )
  // [
  //   {
  //     from: { key, value },
  //     to: { key, value },
  //   }
  // ]
  /* prepare shape of output */
  const output = links
    .reduce((acc, line) => acc.concat([line.from.key, line.to.key]), [])
    .reduce((acc, key) => {
      if (key in acc) return acc
      acc[key] = { key, previous: null }
      return acc
    }, {})

  for (const { from, to } of links) {
    /* fill out the value */
    if ('value' in from) {
      /* check existing value is not in conflict */
      if ('data' in output[from.key] && !isEqual(output[from.key].data, from.value)) {
        throw ValueError(from.key, output[from.key].data, from.value)
      }

      output[from.key].data = from.value
    }
    if ('value' in to) {
      /* check existing value is not in conflict */
      if ('data' in output[to.key] && !isEqual(output[to.key].data, to.value)) {
        throw ValueError(to.key, output[to.key].data, to.value)
      }

      output[to.key].data = to.value
    }

    /* fill out the previous */
    if (!output[to.key].previous) output[to.key].previous = []
    output[to.key].previous.push(from.key)
  }

  /* fill out the history */
  Object.keys(output).forEach(key => createHistory(output, key))

  /* add default node.data = {} */
  Object.keys(output).forEach(key => {
    if (!('data' in output[key])) output[key].data = {}
  })

  if (typeof opts.dataField === 'string') {
    Object.keys(output).forEach(key => {
      const data = output[key].data
      output[key].data = {
        [opts.dataField]: data
      }
    })
  }

  return output
}

function ValueError (key, valA, valB) {
  const a = json5.stringify(valA)
  const b = json5.stringify(valB)
  return new Error(`node ${key} has two contradicting values set: ${a}, ${b}`)
}

function parseKeyValue (idValue) {
  const key = idValue.match(/^\w+/)[0]

  const valueMatch = idValue.match(/\[([^\]]+)\]$/) // the contents of []

  if (!valueMatch) return { key }

  let value = valueMatch[1]
  try {
    value = json5.parse(value)
  } catch (err) {
    value = json5.parse(`"${value}"`)
    // NOTE "badger" is not JSON, however "'badger'" is
    // If this turns out to be a bad hack, we can just do nothing in the catch
  }
  return { key, value }
}

// Create the nodeHistory for key and nodes earlier than key.
function createHistory (graph, key) {
  if (graph[key].history) return

  const history = []
  graph[key].history = history

  if (graph[key].previous === null) return

  for (const prevKey of graph[key].previous) {
    if (!history.includes(prevKey)) history.push(prevKey)

    createHistory(graph, prevKey)
    for (const prev of graph[prevKey].history) {
      if (history.includes(prev)) continue

      history.push(prev)
    }
  }
  // Note: assumes there are no cycles.
}

function buildGraph (mermaid, opts = {}) {
  const nodes = buildNodes(mermaid, opts)

  return new Graph(Object.values(nodes))
}

module.exports = {
  buildGraph,
  buildNodes
}
