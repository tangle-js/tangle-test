# @tangle/test

## Example Usage

To build a graph of nodes like:
```
   A
  / \
 B   C
  \ /
   D
```
_assuming direction is top->bottom_


```js
const { buildNodes, buildGraph } = require('@tangle/test')

const nodes = buildNodes(`
  A-->B[{ name: felix }]
  A-->C
  B-->D
  C-->D
`)

// => {
//   A: {
//     key: 'A',
//     data: {},
//     previous: null,
//     history: []
//   },
//   B: {
//     key: 'B',
//     data: { name: felix }, 
//     previous: ['A'],
//     history: ['A']
//   },
//   C: {
//     key: 'C',
//     data: {},
//     previous: ['A'],
//     history: ['A']
//   },
//   D: {
//     key: 'D',
//     data: {},
//     previous: ['B', 'C'],
//     history: ['B','A','C']
//   }
// }

```js
const graph = buildGraph(`
  A-->B[{ name: felix }]
  A-->C
  B-->D
  C-->D
`)
// => @tangle/graph instance

graph.getBacklinks('B')
// => ['A']
```

## API

### `buildNodes(mermaid, opts) => nodes`

where:
- `mermaid` *String* - a mermaid-js like graph string
- `opts` *Object* - optional
    - `opts.dataField` *String* optionally nest the data input under a `node.data[dataField]`
- `nodes` *Object* - the nodes described in your mermaid diagram, format `{ nodeId: node }`

### `buildGraph(mermaid, opts) => graph`

where:
- `mermaid` *String* - a mermaid-js like graph string
- `opts` *Object* - optional
    - `opts.dataField` *String* optionally nest the data input under a `node.data[dataField]`
- `graph` - a `@tangle/graph` instance


## Features

### Draw arrows however

- `A-->B`
- `A --> B`
- `A -> B`
- `A > B`

### Chain arrows

```
  A-->B-->C
```

this is the same as
```
  A-->B
  B-->C
```

Combining this with indenting can make it a lot easier to visualise branching graphs:
```
  A-->B-->C------>Z
      B-->X-->Y-->Z
```

### JSON5 values

we parse values with `json5`, so you can do things like

```
  A[null] --> B[{ set: 'dog' }] --> C[100]
```

and you'll get the value your would expect (not just strings).

NOTE:
- `json5` means you don't need to wrap everything in ""
- Strings will still come back as strings
- this feature might have spicy unresolved edge cases!
