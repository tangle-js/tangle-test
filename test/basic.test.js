const test = require('tape')
const { buildNodes } = require('../')

test('basic', t => {
  const nodes = buildNodes(`
    A-->B
    A-->C
    B-->D
    C-->D
  `)

  t.deepEqual(
    nodes,
    {
      A: {
        key: 'A',
        data: {},
        previous: null,
        history: []
      },
      B: {
        key: 'B',
        data: {},
        previous: ['A'],
        history: ['A']
      },
      C: {
        key: 'C',
        data: {},
        previous: ['A'],
        history: ['A']
      },
      D: {
        key: 'D',
        data: {},
        previous: ['B', 'C'],
        history: ['B', 'A', 'C']
      }
    }
  )

  t.end()
})
