const test = require('tape')
const { buildNodes } = require('../')

test('history', t => {
  const nodes = buildNodes(`
    A-->B
    A-->C
    B-->D
    C-->D
    D-->E
    E-->F
    D-->F
    F-->G
    F-->H
  `)

  t.deepEqual(
    nodes,
    {
      A: {
        key: 'A',
        data: {},
        previous: null,
        history: []
      },
      B: {
        key: 'B',
        data: {},
        previous: ['A'],
        history: ['A']
      },
      C: {
        key: 'C',
        data: {},
        previous: ['A'],
        history: ['A']
      },
      D: {
        key: 'D',
        data: {},
        previous: ['B', 'C'],
        history: ['B', 'A', 'C']
      },
      E: {
        key: 'E',
        data: {},
        previous: ['D'],
        history: ['D', 'B', 'A', 'C']
      },
      F: {
        key: 'F',
        data: {},
        previous: ['E', 'D'],
        history: ['E', 'D', 'B', 'A', 'C']
      },
      G: {
        key: 'G',
        data: {},
        previous: ['F'],
        history: ['F', 'E', 'D', 'B', 'A', 'C']
      },
      H: {
        key: 'H',
        data: {},
        previous: ['F'],
        history: ['F', 'E', 'D', 'B', 'A', 'C']
      }
    }
  )

  t.end()
})
