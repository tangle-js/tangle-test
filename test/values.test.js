const test = require('tape')
const { buildNodes } = require('../')

test('values', t => {
  let nodes = buildNodes(`
    A[cat]-->B[dog]
  `)

  t.deepEqual(
    nodes,
    {
      A: {
        key: 'A',
        data: 'cat',
        previous: null,
        history: []
      },
      B: {
        key: 'B',
        data: 'dog',
        previous: ['A'],
        history: ['A']
      }
    },
    'simple case'
  )

  t.throws(
    () => {
      nodes = buildNodes(`
        A-->B[dog]
        B[cat]-->C
      `)
    },
    /node B has two contradicting values set/,
    'throws when value is set twice'
  )

  t.end()
})

test('values (opts.dataField)', t => {
  const nodes = buildNodes(
    `
      A[cat]-->B[dog]
    `,
    { dataField: 'animal' }
  )

  t.deepEqual(
    nodes,
    {
      A: {
        key: 'A',
        data: {
          animal: 'cat'
        },
        previous: null,
        history: []
      },
      B: {
        key: 'B',
        data: {
          animal: 'dog'
        },
        previous: ['A'],
        history: ['A']
      }
    },
    'data.animal!'
  )

  t.end()
})
