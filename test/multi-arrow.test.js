const test = require('tape')
const { buildNodes } = require('../')

//  A
//  |
//  B
//  | \
//  C  D

test('multi-arrow', t => {
  const nodes = buildNodes(`
    A-->B[badger]-->C
        B---------->D
  `)

  t.deepEqual(
    nodes,
    {
      A: {
        key: 'A',
        data: {},
        previous: null,
        history: []
      },
      B: {
        key: 'B',
        data: 'badger',
        previous: ['A'],
        history: ['A']
      },
      C: {
        key: 'C',
        data: {},
        previous: ['B'],
        history: ['B', 'A']
      },
      D: {
        key: 'D',
        data: {},
        previous: ['B'],
        history: ['B', 'A']
      }
    }
  )

  t.end()
})
