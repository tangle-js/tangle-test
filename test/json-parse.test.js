const test = require('tape')
const { buildNodes } = require('../')

//  A
//  |
//  B
//  | \
//  C  D

test('json-parse', t => {
  const nodes = buildNodes(`
    A[null]-->B[{ set: 'dog' }]-->C[100]
  `)

  t.deepEqual(
    nodes,
    {
      A: {
        key: 'A',
        data: null,
        previous: null,
        history: []
      },
      B: {
        key: 'B',
        data: { set: 'dog' },
        previous: ['A'],
        history: ['A']
      },
      C: {
        key: 'C',
        data: 100,
        previous: ['B'],
        history: ['B', 'A']
      }
    }
  )

  t.end()
})
