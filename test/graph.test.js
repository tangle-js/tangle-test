const test = require('tape')
const { buildGraph, buildNodes } = require('../')

test('graph', t => {
  const graph = buildGraph(`
    A-->B
    A-->C
    B-->D
    C-->D
  `)

  t.deepEqual(
    graph.getBacklinks('D'),
    ['B', 'C'],
    'Can getBacklinks of a node'
  )

  const nodes = buildNodes(`
    A-->B
    A-->C
    B-->D
    C-->D
  `)
  t.deepEqual(
    graph.getNode('D'),
    nodes.D,
    'The node in the graph matches tangle-test'
  )

  t.end()
})
